
SRC=rain.c sw_uart.c print.c
TARGET=rain

CC=avr-gcc
OBJCOPY=avr-objcopy
SIZE=avr-size
STRIP=avr-strip
CFLAGS=-Os -mmcu=attiny85 -DF_CPU=8000000 -DPRINT_DEC

#CFLAGS+= -mcall-prologues # Actually increase size
CFLAGS+= -Wall
CFLAGS+= -funsigned-char
CFLAGS+= -funsigned-bitfields
CFLAGS+= -fpack-struct
CFLAGS+= -fshort-enums
CFLAGS+= -ffreestanding
CFLAGS+= -fno-tree-scev-cprop
CFLAGS+= -ffunction-sections
CFLAGS+= -fdata-sections
CFLAGS+= -Wstrict-prototypes
CFLAGS+= -Wa,-adhlns=$(<:.c=.lst)
#CFLAGS+= --param inline-call-cost=2 # saves some space
CFLAGS+= -finline-limit=3
CFLAGS+= -fno-inline-small-functions
#CFLAGS+= -fwhole-program # seems to do most
CFLAGS+= -flto
CFLAGS+= -I. -Iavr-libs/include

LDFLAGS= -Wl,--relax,--gc-sections,-Map=$(<:.c=.map),--cref -lm -flto

all: $(TARGET).hex

$(TARGET).elf: $(SRC)
	$(CC) $(CFLAGS) $(LDFLAGS) $(SRC) -o $@

%hex: %elf
	$(STRIP) -s $<
	$(SIZE) $<
	$(OBJCOPY) -O ihex -R .fuse $< $@

clean:
	rm -f *.hex *.elf *.o
