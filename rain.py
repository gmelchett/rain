#!/usr/bin/env python3

import sys

t = '0'
count = 0
start = False
msg = ""

def parity(u):
    p = 0
    for i in range(8):
        if ((u >> i) & 0x1) == 1:
            p += 1
    return p & 1

for l in open(sys.argv[1]).read().split("\n")[2:]:

    m = l[3:].replace(" ", "")
    for q in m:
        if not start and q == '0':
            continue
        start = True
        msg += q


i = 0

while msg[i] == '0':
    i+= 1

bidx = 7
byte = 0
while i < len(msg):

    while msg[i] == '1':
        i+=1

    bl = 0

    while msg[i] == '0':
        i+= 1
        bl += 1

    if bl < 50:
        bit = 0 
    elif bl < 100:
        bit = 1
    else:
        print("msg start")
        byte = 0
        bidx = 7
        continue

    byte |= bit << bidx;
    bidx -= 1
    if bidx < 0:
        print("0x%02x " % byte, end="")
        bidx = 7
        byte = 0





