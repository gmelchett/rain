
# Reverse engineering of a cheap rain meter

I took apart a cheap wireless rain meter bought from a local chain, [Biltema](http://www.biltema.se/sv/Kontor---Teknik/Klockor-och-vaderstation/Vaderstation-och-termometer/Tradlos-regnmatare-2000036033/)


The inside the rain meter consists of two PCBs, one sensor board and one 433 MHz transmitter board.
On the transmitter board, there are convenient markings, VCC, DATA and GND.
I desoldered the 433 MHz transmitter board.

## Physical layer

Each minute the rain meter sends data. Looking at the raw data, you see that it looks like a pattern that gets
repeated seven times each minute.
The data frequency about is 500 Hz.

After looking at the pattern, I figured out that what counts is the mount of silence between each transmitted "1" that decides that
matters.

I've figured out three different states:
| ms | Meaning |
|----:|--------|
| < 2.5 | 0 |
| > 2.5 and < 5.0 | 1 |
| > 5.0 | but not too long, it signs packet start. Reset/Power on is somewhat simular |
 
## Protocol format

I expected the protocol to include some fixed magic value and a checksum, but I was wrong. Neither is present.

Each packet is four bytes long, and repeated seven times each minute:

 * Byte 0:
 Seems to differ at each power on. But once the "rain meter" is powered on, it stays the same. Maybe there to
 differ between you rain meter and your neighbors?
 * Byte 1:
 7-4 Upper 4 bits of rain measured
 * Byte 1:
 3-0 Upper 4 bits of temperature measured
 * Byte 2:
 7-0 Temperature, lower 8 bits. Temperature in degree C * 10
 * Byte 3:
 7-0 Amount of rain. Increases with 25 (decimal) each time the bowl gets full and it flips.
```
Byte 0       Byte 1                                             Byte 2                 Byte 3
+------------+-------------------------+------------------------+----------------------+---------------+
| Unknown    | 7-4 Rain, upper 4 bits  | 3-0 Temp, upper 4 bits | Temperature, lower 8 | Rain, lower 8 |
+------------+-------------------------+------------------------+----------------------+---------------+
```
## How much rain falls

The bowl gets flipped once about 4-5g of water has been collected. The collection area is roughly 3cm x 10cm,
which means that 1.667mm of rain over 30 cm2 weights about 5g.
I think I've missed something here, since the sensor reports 25 (unknown unit) Some calibration against
and old fashioned rain meter is needed.
I have to go and buy a old fashioned rain meter and do some calbration - after I've built some hardware.






