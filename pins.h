#pragma once

#include <avr/io.h>

#define RAIN_PIN PB2
#define RAIN_DDR DDRB
#define RAIN_PORT PORTB

#define UART_PORT PORTB
#define UART_DDR DDRB
#define UART_TX PB3
