
#include "main.h"
#include "pins.h"
#include "print.h"

#include <avr/io.h>

#define RAIN_DELAY 50 /* 20 khz sample rate */

#define CMD_TYPE_ZERO 50
#define CMD_TYPE_ONE 100
#define CMD_TYPE_NO_CMD 200

static u8 receive_weather_data(bool *nodata)
{
	u8 d = 0;
	int bidx = 7;

	do {
		int cmd_len = 0;
		u8 b;

		while (((RAIN_PORT >> RAIN_PIN) & 1) == 1)
			_delay_us(RAIN_DELAY);

		while (((RAIN_PORT >> RAIN_PIN) & 1) == 0) {
			cmd_len++;
			_delay_us(RAIN_DELAY);

			if (cmd_len > 10*CMD_TYPE_NO_CMD) {
				(*nodata) = true;
				return 0;
			}
		}

		if (cmd_len < CMD_TYPE_ZERO) {
			b = 0;
		} else if (cmd_len < CMD_TYPE_ONE) {
			b = 1;
		} else {
			d = 0;
			bidx = 7;
			continue;
		}

		d |= b << bidx;
		bidx--;
	} while (bidx >= 0);

	(*nodata) = false;
	return d;
}

void main(void)
{
	uart_init();
	print("Rain meter started\r\n");

	while (true) {
		int i;
		bool nodata = false;
		u8 pkg[4];

		while (((RAIN_PORT >> RAIN_PIN) & 1) == 0)
			_delay_us(RAIN_DELAY);

		for (i = 0; i < 4; i++) {
			pkg[i] = receive_weather_data(&nodata);
			if (nodata)
				break;
		}

		for (int j = 0; j < i; j++) {
			print("%y ", pkg[j]);
		}
		print("\r\n");
	}
}
